import express from 'express';
import * as auth from '../../../lib/auth/middleware';
import * as controller from './russik.controller';

const router = express.Router();

router.post('/',  controller.read);

export default router;